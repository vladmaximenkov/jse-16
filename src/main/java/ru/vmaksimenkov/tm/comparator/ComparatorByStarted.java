package ru.vmaksimenkov.tm.comparator;

import ru.vmaksimenkov.tm.api.entity.IHasDateStart;

import java.util.Comparator;

public final class ComparatorByStarted implements Comparator<IHasDateStart> {

    private static final ComparatorByStarted INSTANCE = new ComparatorByStarted();

    private ComparatorByStarted(){
    }

    public static ComparatorByStarted getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(final IHasDateStart o1, final IHasDateStart o2) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getDateStart() == null && o2.getDateStart() == null) return 0;
        if (o1.getDateStart() == null && o2.getDateStart() != null) return 1;
        if (o2.getDateStart() == null && o1.getDateStart() != null) return -1;
        return o1.getDateStart().compareTo(o2.getDateStart());
    }

}
