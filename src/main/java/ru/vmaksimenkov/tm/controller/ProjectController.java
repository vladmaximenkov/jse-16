package ru.vmaksimenkov.tm.controller;

import ru.vmaksimenkov.tm.api.controller.IProjectController;
import ru.vmaksimenkov.tm.api.service.IProjectService;
import ru.vmaksimenkov.tm.api.service.IProjectTaskService;
import ru.vmaksimenkov.tm.enumerated.Sort;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.exception.empty.EmptyNameException;
import ru.vmaksimenkov.tm.exception.entity.ProjectNotFoundException;
import ru.vmaksimenkov.tm.exception.entity.SortNotFoundException;
import ru.vmaksimenkov.tm.exception.system.IndexIncorrectException;
import ru.vmaksimenkov.tm.model.Project;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

import static ru.vmaksimenkov.tm.util.TerminalUtil.dashedLine;
import static ru.vmaksimenkov.tm.util.ValidationUtil.*;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    private final IProjectTaskService projectTaskService;

    public ProjectController(final IProjectService projectService, final IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    public void showList() {
        System.out.println("[PROJECT LIST]");
        System.out.println(
            String.format("\t| %-36s | %-12s | %-20s | %-30s | %-30s | %-30s |", "ID", "STATUS", "NAME", "CREATED", "STARTED", "FINISHED")
        );
        int index = 1;
        for (final Project project : projectService.findAll()) {
            System.out.println(index + ".\t" + project);
            index++;
        }
    }

    @Override
    public void showListSorted() {
        System.out.println("[PROJECT LIST SORTED]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        List<Project> projects;
        try {
            projects = projectService.findAll(Sort.getSort(TerminalUtil.nextLine()).getComparator());
        } catch (SortNotFoundException e) {
            System.err.println(e.getMessage() + " Default sort instead");
            projects = projectService.findAll();
        }
        System.out.println(
            String.format("\t| %-36s | %-12s | %-20s | %-30s | %-30s | %-30s |", "ID", "STATUS", "NAME", "CREATED", "STARTED", "FINISHED")
        );
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ".\t" + project);
            index++;
        }
    }

    @Override
    public void create() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        if (isEmpty(name)) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        projectService.add(name, description);
    }

    @Override
    public void clear() {
        System.out.println("[PROJECT CLEAR]");
        projectTaskService.clearTasks();
    }

    private void showProject(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + project.getStatus().getDisplayName());
        System.out.println("CREATED: " + project.getCreated());
        System.out.println("STARTED: " + project.getDateStart());
        System.out.println("FINISHED: " + project.getDateFinish());
        System.out.print(dashedLine());
    }

    @Override
    public void showProjectById() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        showProject(projectService.findOneById(TerminalUtil.nextLine()));
    }

    @Override
    public void showProjectByName() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        showProject(projectService.findOneByName(TerminalUtil.nextLine()));
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        showProject(projectService.findOneByIndex(TerminalUtil.nextNumber() -1));
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER INDEX:");
        projectTaskService.removeProjectByIndex(TerminalUtil.nextNumber() - 1);
    }

    @Override
    public void removeProjectById() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER ID:");
        projectTaskService.removeProjectById(TerminalUtil.nextLine());
    }

    @Override
    public void removeProjectByName() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER NAME:");
        projectTaskService.removeProjectByName(TerminalUtil.nextLine());
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[ENTER INDEX:]");
        final int index = TerminalUtil.nextNumber() -1;
        if (!checkIndex(index, projectService.size())) throw new IndexIncorrectException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        if (isEmpty(name)) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        projectService.updateProjectByIndex(index, name, TerminalUtil.nextLine());
    }

    @Override
    public void updateProjectById() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        if (!projectService.existsById(id)) throw new ProjectNotFoundException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        if (isEmpty(name)) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        projectService.updateProjectById(id, name, TerminalUtil.nextLine());
    }

    @Override
    public void updateProjectByName() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        if (!projectService.existsByName(name)) throw new ProjectNotFoundException();
        System.out.println("ENTER NEW NAME:");
        final String nameNew = TerminalUtil.nextLine();
        if (isEmpty(nameNew)) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        projectService.updateProjectByName(name, nameNew, TerminalUtil.nextLine());
    }

    @Override
    public void startProjectById() {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER ID:");
        projectService.startProjectById(TerminalUtil.nextLine());
    }

    @Override
    public void startProjectByName() {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER NAME:");
        projectService.startProjectByName(TerminalUtil.nextLine());
    }

    @Override
    public void startProjectByIndex() {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER INDEX:");
        projectService.startProjectByIndex(TerminalUtil.nextNumber() -1);
    }

    @Override
    public void finishProjectById() {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER ID:");
        projectService.finishProjectById(TerminalUtil.nextLine());
    }

    @Override
    public void finishProjectByName() {
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER NAME:");
        projectService.finishProjectByName(TerminalUtil.nextLine());
    }

    @Override
    public void finishProjectByIndex() {
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER INDEX:");
        projectService.finishProjectByIndex(TerminalUtil.nextNumber() -1);
    }

    @Override
    public void setProjectStatusById() {
        System.out.println("[SET PROJECT STATUS]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        if(!projectService.existsById(id)) throw new ProjectNotFoundException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        projectService.setProjectStatusById(id, Status.getStatus(TerminalUtil.nextLine()));
    }

    @Override
    public void setProjectStatusByName() {
        System.out.println("[SET PROJECT STATUS]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        if (!projectService.existsByName(name)) throw new ProjectNotFoundException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final Project project = projectService.setProjectStatusByName(name, Status.getStatus(TerminalUtil.nextLine()));
    }

    @Override
    public void setProjectStatusByIndex() {
        System.out.println("[SET PROJECT STATUS]");
        System.out.println("ENTER INDEX:");
        final int index = TerminalUtil.nextNumber() -1;
        if (!checkIndex(index, projectService.size())) throw new IndexIncorrectException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        projectService.setProjectStatusByIndex(index, Status.getStatus(TerminalUtil.nextLine()));
    }

}
