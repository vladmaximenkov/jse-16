package ru.vmaksimenkov.tm.controller;

import ru.vmaksimenkov.tm.api.controller.ITaskController;
import ru.vmaksimenkov.tm.api.service.IProjectService;
import ru.vmaksimenkov.tm.api.service.IProjectTaskService;
import ru.vmaksimenkov.tm.api.service.ITaskService;
import ru.vmaksimenkov.tm.enumerated.Sort;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.exception.empty.EmptyNameException;
import ru.vmaksimenkov.tm.exception.entity.SortNotFoundException;
import ru.vmaksimenkov.tm.exception.entity.TaskNotFoundException;
import ru.vmaksimenkov.tm.exception.system.IndexIncorrectException;
import ru.vmaksimenkov.tm.model.Task;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

import static ru.vmaksimenkov.tm.util.TerminalUtil.dashedLine;
import static ru.vmaksimenkov.tm.util.ValidationUtil.*;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    private final IProjectService projectService;

    private final IProjectTaskService projectTaskService;

    public TaskController(final ITaskService taskService, final IProjectService projectService, final IProjectTaskService projectTaskService) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    public void showList() {
        System.out.println("[TASK LIST]");
        System.out.println(
            String.format("\t| %-36s | %-12s | %-20s | %-30s | %-30s | %-30s | %s |", "ID", "STATUS", "NAME", "CREATED", "STARTED", "FINISHED", "PROJECT")
        );
        int index = 1;
        for (final Task task : taskService.findAll()) {
            System.out.println(index + ".\t" + task);
            index++;
        }
    }

    @Override
    public void showListSorted() {
        System.out.println("[TASK LIST SORTED]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        List<Task> tasks;
        try {
            tasks = taskService.findAll(Sort.getSort(TerminalUtil.nextLine().toUpperCase()).getComparator());
        } catch (SortNotFoundException e) {
            System.err.println(e.getMessage() + " Default sort instead");
            tasks = taskService.findAll();
        }
        System.out.println(
            String.format("\t| %-36s | %-12s | %-20s | %-30s | %-30s | %-30s | %s |", "ID", "STATUS", "NAME", "CREATED", "STARTED", "FINISHED", "PROJECT")
        );
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ".\t" + task);
            index++;
        }
    }

    @Override
    public void create() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        if (isEmpty(name)) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        taskService.add(name, TerminalUtil.nextLine());
    }

    @Override
    public void clear() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
    }

    private void showTask(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + task.getStatus().getDisplayName());
        System.out.println("PROJECT ID: " + task.getProjectId());
        System.out.println("CREATED: " + task.getCreated());
        System.out.println("STARTED: " + task.getDateStart());
        System.out.println("FINISHED: " + task.getDateFinish());
        System.out.print(dashedLine());
    }

    @Override
    public void showTaskById() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        showTask(taskService.findOneById(TerminalUtil.nextLine()));
    }

    @Override
    public void showTaskByName() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER NAME:");
        showTask(taskService.findOneByName(TerminalUtil.nextLine()));
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        showTask(taskService.findOneByIndex(TerminalUtil.nextNumber() - 1));
    }

    @Override
    public void showListByProjectId() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        final List<Task> taskList = projectTaskService.findAllTaskByProjectId(TerminalUtil.nextLine());
        System.out.println(
            String.format("\t| %-36s | %-12s | %-20s | %-30s | %-30s | %-30s | %s |", "ID", "STATUS", "NAME", "CREATED", "STARTED", "FINISHED", "PROJECT")
        );
        int index = 1;
        for (final Task task : taskList) {
            System.out.println(index + ".\t" + task);
            index++;
        }
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER INDEX:");
        taskService.removeOneByIndex(TerminalUtil.nextNumber() - 1);
    }

    @Override
    public void removeTaskById() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER ID:");
        taskService.removeOneByName(TerminalUtil.nextLine());
    }

    @Override
    public void removeTaskByName() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER NAME:");
        taskService.removeOneByName(TerminalUtil.nextLine());
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER INDEX:]");
        final int index = TerminalUtil.nextNumber() - 1;
        if (!checkIndex(index, taskService.size())) throw new IndexIncorrectException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        if (isEmpty(name)) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        taskService.updateTaskByIndex(index, name, TerminalUtil.nextLine());
    }

    @Override
    public void updateTaskById() {
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        if (!taskService.existsById(id)) throw new TaskNotFoundException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        if (isEmpty(name)) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        taskService.updateTaskById(id, name, TerminalUtil.nextLine());
    }

    @Override
    public void updateTaskByName() {
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        if (!taskService.existsByName(name)) throw new EmptyNameException();
        System.out.println("ENTER NEW NAME:");
        final String nameNew = TerminalUtil.nextLine();
        if (isEmpty(nameNew)) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        taskService.updateTaskByName(name, nameNew, TerminalUtil.nextLine());
    }

    @Override
    public void startTaskById() {
        System.out.println("[START TASK]");
        System.out.println("ENTER ID:");
        taskService.startTaskById(TerminalUtil.nextLine());
    }

    @Override
    public void startTaskByName() {
        System.out.println("[START TASK]");
        System.out.println("ENTER NAME:");
        taskService.startTaskByName(TerminalUtil.nextLine());
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("[START TASK]");
        System.out.println("ENTER INDEX:");
        taskService.startTaskByIndex(TerminalUtil.nextNumber() - 1);
    }

    @Override
    public void finishTaskById() {
        System.out.println("[START TASK]");
        System.out.println("ENTER ID:");
        taskService.finishTaskById(TerminalUtil.nextLine());
    }

    @Override
    public void finishTaskByName() {
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER NAME:");
        taskService.finishTaskByName(TerminalUtil.nextLine());
    }

    @Override
    public void finishTaskByIndex() {
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER INDEX:");
        taskService.finishTaskByIndex(TerminalUtil.nextNumber() - 1);
    }

    @Override
    public void setTaskStatusById() {
        System.out.println("[SET TASK STATUS]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        if (!taskService.existsById(id)) throw new TaskNotFoundException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        taskService.setTaskStatusById(id, Status.getStatus(TerminalUtil.nextLine()));
    }

    @Override
    public void setTaskStatusByName() {
        System.out.println("[SET TASK STATUS]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        if (!taskService.existsByName(name)) throw new TaskNotFoundException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        taskService.setTaskStatusByName(name, Status.getStatus(TerminalUtil.nextLine()));
    }

    @Override
    public void setTaskStatusByIndex() {
        System.out.println("[SET TASK STATUS]");
        System.out.println("ENTER INDEX:");
        final int index = TerminalUtil.nextNumber() - 1;
        if (!checkIndex(index, taskService.size())) throw new IndexIncorrectException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        taskService.setTaskStatusByIndex(index, Status.getStatus(TerminalUtil.nextLine()));
    }

    @Override
    public void bindTaskByProjectId() {
        System.out.println("[BIND TASK TO PROJECT BY ID]");
        if (taskService.size() < 1) throw new TaskNotFoundException();
        System.out.println("ENTER TASK INDEX:");
        final String taskId = taskService.getIdByIndex(TerminalUtil.nextNumber() - 1);
        System.out.println("ENTER PROJECT ID:");
        projectTaskService.bindTaskByProjectId(TerminalUtil.nextLine(), taskId);
    }

    @Override
    public void unbindTaskById() {
        System.out.println("[UNBIND TASK BY ID]");
        if (taskService.size() < 1) throw new TaskNotFoundException();
        System.out.println("ENTER TASK ID:");
        projectTaskService.unbindTaskFromProject(TerminalUtil.nextLine());
    }

}
