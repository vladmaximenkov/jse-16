package ru.vmaksimenkov.tm.enumerated;

import ru.vmaksimenkov.tm.exception.entity.StatusNotFoundException;

import static ru.vmaksimenkov.tm.util.ValidationUtil.checkStatus;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETE("Complete");

    Status(String displayName) {
        this.displayName = displayName;
    }

    private String displayName;

    public String getDisplayName() {
        return displayName;
    }

    public static Status getStatus(String s) {
        s = s.toUpperCase();
        if (!checkStatus(s)) throw new StatusNotFoundException();
        return Status.valueOf(s);
    }

}
